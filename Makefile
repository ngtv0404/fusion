export
include config.mk

.PHONY: all headers clean

all:
	$(MAKE) -C kernel
	$(MAKE) -C initrd -B

clean:
	$(MAKE) clean -C kernel
	$(MAKE) clean -C initrd
	rm -rf isodir
	rm -f fusion.iso
	rm -f kernel.img
	rm -f kernel7.img
	rm -f kernel8.img
	rm -f kernel/disasm.txt

i386-pc-iso: all
	mkdir -p isodir/boot/grub
	cp kernel/kernel.elf isodir/boot
	cp initrd/initrd.img isodir/boot
	cp mktool/i386-pc-iso/stage2_eltorito isodir/boot/grub
	cp mktool/i386-pc-iso/menu.lst isodir/boot/grub
	mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -V FusionCD -input-charset utf-8 -o fusion.iso isodir

i386-pc-qemu: i386-pc-iso
	qemu-system-i386 -cdrom fusion.iso -soundhw pcspk

i386-pc-gdb: i386-pc-iso
	qemu-system-i386 -cdrom fusion.iso -soundhw pcspk -S -gdb tcp::9000 &
	gdb-multiarch --command=gdb_cmds.txt
