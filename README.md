# Fusion - a hobby operating system written from scratch
## What is Fusion?
Fusion is a portable operating system in development by Succboy6000 (Thanh Vinh Nguyen). Currently, it's in active development, with functions still being added.
As of now, Fusion only supports x86.
## How to clone and build
To clone Fusion, use this command:
```
git clone --recursive https://gitlab.com/ngtv0404/fusion.git
```
Then, assuming a GCC cross-compiler targeting `i686-elf` is installed and `mkisofs`, `qemu`, `nasm` and `imdisk` (if you're using Windows) are installed, it's just a matter of:

    make i386-pc-qemu
**NOTE:** Windows Subsystem for Linux is unsupported as of now; use Cygwin if you're intending to build Fusion on a Windows host (or dual-boot Linux if you so choose).
## List of implemented features (for `i386-pc`)
 - ACPI (using [LAI](https://github.com/qword-os/lai))
 - Paging
 - PCI (untested)
 - PIC
 - PIT
 - ISA DMA (untested)
 - CMOS/RTC
 - Framebuffer (using VBE)
 - GDT/IDT
 - Real Mode interrupts calling in Protected Mode (using code written by Napalm@rohitab)
 - 8042 PS/2 controller
 - Round-robin preemptive multitasking
 - FAT12 (for initrd)
 - VFS
 - Memory map parsing (using Multiboot)
 - Kernel heap
