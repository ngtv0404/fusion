/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.	If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <path.h>

int main(int argc, char **argv) {
  if(argc > 2) printf("cd: too many arguments\n");
  else if(argc == 1) path_setcwd("/");
  else path_setcwd(argv[1]);
  return 0;
}
