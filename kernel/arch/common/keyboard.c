/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <keyboard.h>
#include <kblayouts/qwerty_us.h>
#include <power.h>

volatile uint8_t kb_stattab[256] = {0};
volatile uint16_t kb_modstat = 0;
kb_layout_t *kb_curr_layout = &qwerty_us;
uint32_t kb_buffer[KB_BUFSIZE] = {0};
volatile uint8_t kb_bufptr = 0;

void kb_register(uint8_t keycode, uint8_t state) {
	uint8_t last_state = kb_stattab[keycode]; // used when handling latching keys
	kb_stattab[keycode] = state;
	uint32_t key = kb_curr_layout->default_map[keycode]; // get what the keycode means
	if(state) {
		/* pressed */
		if(!last_state) {
			/* this part handles the latching keys */
			switch(key) {
				case KB_NUM_LOCK: kb_modstat ^= (1 << 0); return;
				case KB_CAPS_LOCK: kb_modstat ^= (1 << 1); return;
				case KB_SCROLL_LOCK: kb_modstat ^= (1 << 2); return;
				default: break;
			}
		}
		/* this part handles the Ctrl, Alt and Shift keys */
		uint8_t is_cas = 0;
		switch(key) {
			case KB_CTRL_L: kb_modstat |= (1 << 3); is_cas = 1; break;
			case KB_CTRL_R: kb_modstat |= (1 << 4); is_cas = 1; break;
			case KB_ALT_L: kb_modstat |= (1 << 5); is_cas = 2; break;
			case KB_ALT_R: kb_modstat |= (1 << 6); is_cas = 2; break;
			case KB_SHIFT_L: kb_modstat |= (1 << 7); is_cas = 3; break;
			case KB_SHIFT_R: kb_modstat |= (1 << 8); is_cas = 3; break;
			default: break;
		}
		if(is_cas) {
			kb_modstat |= (1 << (8 + is_cas));
			return;
		}
		if(kb_bufptr == KB_BUFSIZE) return; // we don't have any more space for storing keypresses
		/* modify the keypress according to modifier keys */
		for(uint16_t i = 0; i < kb_curr_layout->mods_cnt; i++) {
			if((kb_modstat & kb_curr_layout->mods[i]->bitmask) == kb_curr_layout->mods[i]->mask_res) {
				uint8_t modified = 0;
				for(uint16_t j = 0; j < kb_curr_layout->mods[i]->ents; j++) {
					if(kb_curr_layout->mods[i]->entries[j].from == key) {
						key = kb_curr_layout->mods[i]->entries[j].to;
						modified = 1;
						break;
					}
				}
				if(modified) break;
			}
		}

		kb_buffer[kb_bufptr++] = key;
	} else {
		/* released */
		/* this part handles the Ctrl, Alt and Shift keys */
		uint8_t is_cas = 0;
		switch(key) {
			case KB_CTRL_L: kb_modstat &= ~(1 << 3); is_cas = 1; break;
			case KB_CTRL_R: kb_modstat &= ~(1 << 4); is_cas = 1; break;
			case KB_ALT_L: kb_modstat &= ~(1 << 5); is_cas = 2; break;
			case KB_ALT_R: kb_modstat &= ~(1 << 6); is_cas = 2; break;
			case KB_SHIFT_L: kb_modstat &= ~(1 << 7); is_cas = 3; break;
			case KB_SHIFT_R: kb_modstat &= ~(1 << 8); is_cas = 3; break;
			default: break;
		}
		if(is_cas) {
			kb_modstat &= ~(1 << (8 + is_cas));
			kb_modstat |= (kb_modstat & ((1 << (3 + (2 * is_cas))) | (1 << (4 + (2 * is_cas))))) ? (1 << (8 + is_cas)) : 0;
			return;
		}
	}
}

uint8_t kb_status(void) {
	return (kb_bufptr) ? 1 : 0;
}

uint32_t kb_getkey(void) {
	uint32_t ret = kb_buffer[0];
	for(uint8_t i = 1; i < KB_BUFSIZE; i++) kb_buffer[i - 1] = kb_buffer[i];
	kb_buffer[KB_BUFSIZE - 1] = 0;
	if(kb_bufptr) kb_bufptr--;
	return ret;
}

void kb_clear(void) {
	kb_bufptr = 0;
}

void kb_postreg(void) {
	if((kb_modstat & ((1 << 3) | (1 << 4))) && (kb_modstat & ((1 << 5) | (1 << 6))) && kb_buffer[kb_bufptr - 1] == KB_DEL) {
		kb_bufptr--;
		power_btnhandler();
	}
}
