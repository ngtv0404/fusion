/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <power.h>
#include <stdio.h>
#include <vfs.h>
#include <task.h>
#include <textmode.h>
#include <framebuffer.h>
#include <string.h>
#include <stdlib.h>

volatile uint8_t power_btnhandler_inservice = 0;

void power_btnhandler(void) {
	if(power_btnhandler_inservice) return; // already in service (called due to multiple pressing)
	task_pause = 1; // pause task yielding so that the cursor position remains fixed
	power_btnhandler_inservice = 1;
	printf("Power button pressed\n[S]hutdown, [R]eboot, [C]ancel\n");
	uint32_t sel = 0;
	vfs_open(stdin, 0);
	while(sel != 's' && sel != 'r' && sel != 'c') {
		sel = getc(stdin);
		if(sel >= 'A' && sel <= 'Z') sel += 32;
	}
	switch(sel) {
		case 's': power_shutdown(); break;
		case 'r': power_reboot(); break;
		default: break;
	}
	task_pause = 0;
}
