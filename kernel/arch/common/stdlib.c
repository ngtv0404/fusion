/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <pfa.h>
#include <paging.h>
#include <string.h>
#include <stdio.h>
#include <task.h>

int atoi(const char *str) {
	if((str == NULL) || (*str == '\0')) return 0;
	uint32_t i = 0;
	int sign = 1, res = 0;
	if(str[0] == '-') {
		sign = -1;
		i++;
	}
	for(; str[i] != 0; i++) {
		if((str[i] < '0') || (str[i] > '9')) {
			return 0;
		}
		res = (res * 10) + (str[i] - '0');
	}
	return sign * res;
}

static void reverse(char str[], uint32_t length) {
	uint32_t start = 0, end = length - 1;
	while(start < end) {
		char t = *(str + start);
		*(str + start) = *(str + end);
		*(str + end) = t;
		start++;
		end--;
	}
}

char* itoa(int num, char* str, uint8_t base) {
	uint32_t i = 0;
	uint8_t is_neg = 0;

	if(num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	if(num < 0 && base == 10) {
		is_neg = 1;
		num = -num;
	}

	while(num != 0) {
		uint32_t rem = num % base;
		str[i++] = (rem > 9) ? ((rem - 10) + 'a') : (rem + '0');
		num /= base;
	}

	if(is_neg) str[i++] = '-';
	str[i] = 0;

	reverse(str, i);
	return str;
}

char* itoa_unsigned(uint32_t num, char* str, uint8_t base) {
	uint32_t i = 0;

	if(num == 0) {
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}

	while(num != 0) {
		uint32_t rem = num % base;
		str[i++] = (rem > 9) ? ((rem - 10) + 'a') : (rem + '0');
		num /= base;
	}

	str[i] = 0;

	reverse(str, i);
	return str;
}

/*
	NEW: Dynamic memory allocation.
	Memory will be organized in this way:
	+----+------+----+------+------+----+------+---+------+----+------+
	|Info|Header|Data|Footer|Header|Data|Footer|...|Header|Data|Footer|
	+----+------+----+------+------+----+------+---+------+----+------+
	     ------1 block-------
*/
extern uint32_t kernel_end; // the address of the end of the kernel
heap_info_t *heap_info = NULL; // we will place our heap right at the end of the kernel

void heap_init(uintptr_t addr, uintptr_t init_size, uintptr_t max_size) {
	init_size += sizeof(heap_info_t);
	if(init_size & 0xFFF) {
		init_size &= ~0xFFF;
		init_size += 0x1000;
	}
	init_size -= sizeof(heap_info_t);
	for(uintptr_t i = 0; i < (init_size + sizeof(heap_info_t)); i += 0x1000) {
		uintptr_t frame = pfa_freeframe();
		if(frame == UINTPTR_MAX) return;
		paging_map(frame << 12, addr + i, 4096, 0, 1);
	}
	heap_info = (heap_info_t*) addr;
	/* set up heap information */
	heap_info->size = init_size;
	heap_info->max_size = max_size;
	/* set up the first memory block */
	heap_header_t *header = (heap_header_t*) ((uintptr_t) heap_info + sizeof(heap_info_t));
	header->in_use = 0;
	header->size = init_size - (sizeof(heap_header_t) + sizeof(heap_footer_t));
	heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size);
	footer->header = header;
}

static int heap_expand(uintptr_t size) {
	size += sizeof(heap_header_t) + sizeof(heap_footer_t);
	if((heap_info->size + size) >= heap_info->max_size) return -1; // we've hit the maximum size limit
	if(((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size + size) & 0xFFF) {
		uint32_t aligned = (uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size + size;
		aligned &= ~0xFFF;
		aligned += 0x1000;
		size += aligned - ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size + size);
	}
	for(uintptr_t i = 0; i < size; i += 0x1000) {
		uintptr_t frame = pfa_freeframe();
		if(frame == UINTPTR_MAX) return -2;
		paging_map(frame << 12, (uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size + i, 4096, 0, 1);
	}
	/* set up new memory block */
	heap_header_t *new_header = (heap_header_t*) ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size);
	new_header->in_use = 0;
	new_header->size = size - (sizeof(heap_header_t) + sizeof(heap_footer_t));
	heap_footer_t *new_footer = (heap_footer_t*) ((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size);
	new_footer->header = new_header;
	// printf("expanding, size = %u, new_header = %x, new_footer = %x\n", size, new_header, new_footer);
	heap_info->size += size;
	/* check if we can merge with the preceding block */
	heap_footer_t *pb_footer = (heap_footer_t*) ((uintptr_t) new_header - sizeof(heap_footer_t));
	if(pb_footer->header->in_use == 0) {
		/* yes we can merge it */
		pb_footer->header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + new_header->size;
		new_footer->header = pb_footer->header; // transfer the header
		return 1;
	}
	return 0;
}

static uintptr_t alloc(uintptr_t size, uint32_t alignment, uintptr_t *physaddr) {
	heap_header_t *header = (heap_header_t*) ((uintptr_t) heap_info + sizeof(heap_info_t));
	while(1) {
		if(alignment) {
			/* aligned allocation */
			if(header->in_use == 0) {
				/* we found a free block but we don't know if it's usable or not */
				uintptr_t aligned_ret = (uintptr_t) header + sizeof(heap_header_t); // aligned_ret will hold the aligned address that will be returned to the caller
				if(aligned_ret % alignment) aligned_ret = (aligned_ret / alignment + 1) * alignment; // align the address if needed
				uintptr_t offset = aligned_ret - ((uintptr_t) header + sizeof(heap_header_t)); // how much are we deviating from the original address
				if(header->size >= (offset + sizeof(heap_footer_t) + sizeof(heap_header_t) + size)) {
					/* it's big enough to contain everything */
					if(offset == 0) {
						/* this header is aligned naturally, we don't have to touch it */
						if(header->size > (size + sizeof(heap_header_t) + sizeof(heap_footer_t))) {
							/* we can split the block up */
							heap_footer_t *new_footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + size);
							new_footer->header = header;
							heap_header_t *new_header = (heap_header_t*) ((uintptr_t) new_footer + sizeof(heap_footer_t));
							new_header->in_use = 0;
							new_header->size = header->size - (size + sizeof(heap_header_t) + sizeof(heap_footer_t));
							header->size = size;
							heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size);
							footer->header = new_header;
							if(((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size + sizeof(heap_footer_t)) < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) {
								/* there's a header ahead of us, and we want to know if we can merge this new block with it */
								heap_header_t *nb_header = (heap_header_t*) ((uintptr_t) footer + sizeof(heap_footer_t));
								if(nb_header->in_use == 0) {
									/* we can merge it indeed */
									new_header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + nb_header->size;
									new_footer = (heap_footer_t*) ((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size);
									new_footer->header = new_header;
								}
							}
						}
						header->in_use = 1;
						heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size);
						if(((uintptr_t) footer < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) && (((uintptr_t) footer + sizeof(heap_footer_t) + sizeof(heap_header_t)) >= ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size))) {
							/* we'd better steal all the leftover space */
							uintptr_t new_footer_addr = (uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size - sizeof(heap_footer_t);
							header->size += new_footer_addr - (uintptr_t) footer;
							footer = (heap_footer_t*) new_footer_addr;
							footer->header = header;
						}
						uintptr_t ret = (uintptr_t) header + sizeof(heap_header_t);
						if(physaddr != NULL) *physaddr = paging_getpaddr(ret);
						return ret;
					}
					if(offset > (sizeof(heap_header_t) + sizeof(heap_footer_t))) {
						/* split the block up into 2 */
						uintptr_t orig_size = header->size;
						header->size = offset - (sizeof(heap_footer_t) + sizeof(heap_header_t)); // shrink the block so that it acts like a padding space to make the next block's data portion aligned
						header->in_use = 0;
						heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size); // the footer of the padding block
						footer->header = header;
						if((uintptr_t) header > ((uintptr_t) heap_info + sizeof(heap_info_t))) {
							/* there's a block before us, and we want to know if we can merge it with the padding block */
							heap_footer_t *pb_footer = (heap_footer_t*) ((uintptr_t) header - sizeof(heap_footer_t));
							// printf("pb_footer = 0x%x\n", pb_footer);
							if(pb_footer->header->in_use == 0) {
								/* yes we can merge it */
								pb_footer->header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + header->size; // add the size of the padding block
								footer->header = pb_footer->header; // transfer the header
							}
						}
						heap_header_t *dat_header = (heap_header_t*) ((uintptr_t) footer + sizeof(heap_footer_t)); // the header of the block where we will return to the caller
						// printf("found hdr at 0x%x\n", (uintptr_t) dat_header);
						dat_header->in_use = 1; // we will use this block
						dat_header->size = size;
						heap_footer_t *dat_footer = (heap_footer_t*) ((uintptr_t) dat_header + sizeof(heap_header_t) + dat_header->size); // and its footer
						dat_footer->header = dat_header;
						if(header->size + sizeof(heap_footer_t) + sizeof(heap_header_t) + dat_header->size + sizeof(heap_footer_t) + sizeof(heap_header_t) + sizeof(heap_footer_t) < orig_size) {
							/* split again */
							heap_header_t *unused_hdr = (heap_header_t*) ((uintptr_t) dat_footer + sizeof(heap_footer_t));
							unused_hdr->in_use = 0;
							unused_hdr->size = orig_size - (header->size + sizeof(heap_footer_t) + sizeof(heap_header_t) + dat_header->size + sizeof(heap_footer_t) + sizeof(heap_header_t));
							heap_footer_t *unused_ftr = (heap_footer_t*) ((uintptr_t) unused_hdr + sizeof(heap_header_t) + unused_hdr->size);
							// printf("unused_hdr = 0x%8x, unused_ftr = 0x%8x\n", unused_hdr, unused_ftr);
							unused_ftr->header = unused_hdr;
							if(((uintptr_t) unused_hdr + sizeof(heap_header_t) + unused_hdr->size + sizeof(heap_footer_t)) < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) {
								/* there's a header ahead of us, and we want to know if we can merge this new block with it */
								heap_header_t *nb_header = (heap_header_t*) ((uintptr_t) unused_ftr + sizeof(heap_footer_t));
								if(nb_header->in_use == 0) {
									/* we can merge it indeed */
									unused_hdr->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + nb_header->size;
									unused_ftr = (heap_footer_t*) ((uintptr_t) unused_hdr + sizeof(heap_header_t) + unused_hdr->size);
									unused_ftr->header = unused_hdr;
								}
							}
						}
						if(((uintptr_t) dat_footer < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) && (((uintptr_t) dat_footer + sizeof(heap_footer_t) + sizeof(heap_header_t)) >= ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size))) {
							/* we'd better steal all the leftover space */
							uintptr_t new_footer_addr = (uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size - sizeof(heap_footer_t);
							dat_header->size += new_footer_addr - (uintptr_t) dat_footer;
							dat_footer = (heap_footer_t*) new_footer_addr;
							dat_footer->header = dat_header;
						}
						uintptr_t ret = (uintptr_t) dat_header + sizeof(heap_header_t);
						if(physaddr != NULL) *physaddr = paging_getpaddr(ret);
						// printf("returning 0x%x\n", ret);
						return ret;
					}
				}
			}
		} else {
			if(header->in_use == 0 && header->size >= size) {
				if(header->size > (size + sizeof(heap_header_t) + sizeof(heap_footer_t))) {
					/* we can split the block up */
					heap_footer_t *new_footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + size);
					new_footer->header = header;
					heap_header_t *new_header = (heap_header_t*) ((uintptr_t) new_footer + sizeof(heap_footer_t));
					new_header->in_use = 0;
					new_header->size = header->size - (size + sizeof(heap_header_t) + sizeof(heap_footer_t));
					header->size = size;
					heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size);
					footer->header = new_header;
					if(((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size + sizeof(heap_footer_t)) < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) {
						/* there's a header ahead of us, and we want to know if we can merge this new block with it */
						heap_header_t *nb_header = (heap_header_t*) ((uintptr_t) footer + sizeof(heap_footer_t));
						if(nb_header->in_use == 0) {
							/* we can merge it indeed */
							new_header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + nb_header->size;
							new_footer = (heap_footer_t*) ((uintptr_t) new_header + sizeof(heap_header_t) + new_header->size);
							new_footer->header = new_header;
						}
					}
				}
				header->in_use = 1;
				heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size);
				if(((uintptr_t) footer < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) && (((uintptr_t) footer + sizeof(heap_footer_t) + sizeof(heap_header_t)) >= ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size))) {
					/* we'd better steal all the leftover space */
					uintptr_t new_footer_addr = (uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size - sizeof(heap_footer_t);
					header->size += new_footer_addr - (uintptr_t) footer;
					footer = (heap_footer_t*) new_footer_addr;
					footer->header = header;
				}
				uintptr_t ret = (uintptr_t) header + sizeof(heap_header_t);
				if(physaddr != NULL) *physaddr = paging_getpaddr(ret);
				return ret;
			}
		}
		heap_header_t *last_header = header;
		header = (heap_header_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size + sizeof(heap_footer_t));
		if(((uintptr_t) header + sizeof(heap_header_t)) >= ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) {
			/* we ran out of memory */
			int ret = heap_expand(alignment + size);
			if(ret == -1) return 0; // we can't expand further
			else if(ret == 1) header = last_header; // the last block has been expanded
		}
		// printf("0x%8x, size = %u, used = %u, heap size = 0x%8x, max_size = 0x%8x\n", (uintptr_t) header, header->size, header->in_use, heap_info->size, heap_info->max_size);
	}
}

static void dealloc(uintptr_t addr) {
	heap_header_t *header = (heap_header_t*) (addr - sizeof(heap_header_t));
	header->in_use = 0; // mark this block as unused
	if((uintptr_t) header > ((uintptr_t) heap_info + sizeof(heap_info_t))) {
		/* there's a preceding block */
		heap_footer_t *pb_footer = (heap_footer_t*) ((uintptr_t) header - sizeof(heap_footer_t));
		if(pb_footer->header->in_use == 0) {
			/* yes we can merge it */
			pb_footer->header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + header->size; // add the size of the padding block
			heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size);
			footer->header = pb_footer->header; // transfer the header
		}
	}
	if(((uintptr_t) header + sizeof(heap_header_t) + header->size) < ((uintptr_t) heap_info + sizeof(heap_info_t) + heap_info->size)) {
		/* there's a succeeding block */
		heap_header_t *nb_header = (heap_header_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size + sizeof(heap_footer_t));
		if(nb_header->in_use == 0) {
			/* we can merge it indeed */
			header->size += sizeof(heap_footer_t) + sizeof(heap_header_t) + nb_header->size;
			heap_footer_t *footer = (heap_footer_t*) ((uintptr_t) header + sizeof(heap_header_t) + header->size);
			footer->header = header;
		}
	}
}

void *malloc(size_t size) {
	return (void*) alloc((uintptr_t) size, 0, NULL);
}

void *malloc_ext(size_t size, uint32_t alignment, uintptr_t *physaddr) {
	return (void*) alloc((uintptr_t) size, alignment, physaddr);
}

void free(void *ptr) {
	if(ptr == NULL) return;
	dealloc((uintptr_t) ptr);
}

void *calloc(size_t nitems, size_t size) {
	void *ret = malloc(nitems * size);
	memset(ret, 0, nitems * size);
	return ret;
}

void *realloc(void *ptr, size_t size) {
	if(ptr == NULL) return malloc(size);
	else {
		heap_header_t *header = (heap_header_t*) ((void*) ptr - sizeof(heap_header_t));
		size_t old_size = (size_t) header->size;
		if(size == old_size) return ptr;
		free(ptr);
		void *ret = malloc(size);
		if(ret == ptr) return ret;
		memcpy(ret, ptr, (size > old_size) ? old_size : size);
		return ret;
	}
}

int abs(int i) {
	return ((i < 0) ? -i : i);
}

#define RAND_MAX			32767
uint32_t rand_seed = 0;

uint32_t rand(void) {
	rand_seed = rand_seed * 1103515245 + 12345;
	return (uint32_t)(rand_seed / 65536) % (RAND_MAX + 1);
}

void srand(uint32_t seed) {
	rand_seed = seed;
}

extern int get_retval(); // architecture-specific function to get return value of previous function (a minimal implementation just returns)

__attribute__ ((noreturn)) void exit(void) {
	int ret = get_retval();
	printf("PID %u exited with error code %d\n", task_getpid(), ret);
	while(1); // TODO: implement task deletion
}
