/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#ifdef COMMON_STRING

size_t strlen(const char* str) {
	size_t len = 0;
	while(str[len] != 0) len++;
	return len;
}

int memcmp(const void* aptr, const void* bptr, size_t size) {
	const uint8_t* a = (const uint8_t*) aptr;
	const uint8_t* b = (const uint8_t*) bptr;
	for(size_t i = 0; i < size; i++) {
		if(a[i] < b[i]) return -1;
		else if(b[i] < a[i]) return 1;
	}
	return 0;
}

void* memcpy(void* restrict dstptr, const void* restrict srcptr, size_t size) {
	uint8_t* dst = (uint8_t*) dstptr;
	const uint8_t* src = (const uint8_t*) srcptr;
	for (size_t i = 0; i < size; i++)
		dst[i] = src[i];
	return dstptr;
}

void* memmove(void* dstptr, const void* srcptr, size_t size) {
	uint8_t* dst = (uint8_t*) dstptr;
	const uint8_t* src = (const uint8_t*) srcptr;
	if(dst < src) {
		for(size_t i = 0; i < size; i++) dst[i] = src[i];
	} else {
		for(size_t i = size; i != 0; i--) dst[i - 1] = src[i - 1];
	}
	return dstptr;
}

void* memset(void* bufptr, int value, size_t size) {
	uint8_t* buf = (uint8_t*) bufptr;
	for(size_t i = 0; i < size; i++) buf[i] = (uint8_t) value;
	return bufptr;
}

/* new implementations - TODO: check */
int strcmp(const char* str1, const char* str2) {
	const uint8_t *p1 = (const uint8_t *) str1;
	const uint8_t *p2 = (const uint8_t *) str2;
	uint8_t a = 0, b = 0;
	for(size_t i = 0; ; i++) {
		a = p1[i]; b = p2[i];
		if(a != b || !a || !b) break;
	}
	return (int) (a - b);
}
int strncmp(const void* str1, const void* str2, size_t n) {
	const uint8_t *p1 = (const uint8_t *) str1;
	const uint8_t *p2 = (const uint8_t *) str2;
	uint8_t a = 0, b = 0;
	for(size_t i = 0; i < n; i++) {
		a = p1[i]; b = p2[i];
		if(a != b || !a || !b) break;
	}
	return (int) (a - b);
}

char* strcpy(char* dest, const char* src) {
	return (char*) memcpy((void*) dest, (void*) src, strlen(src) + 1);
}

char *strcat(char *dest, const char *src) {
	memcpy((void*)((uintptr_t) dest + strlen(dest)), (void*) src, strlen(src) + 1);
	return dest;
}

void *memset_16(void *dest, uint16_t val, size_t n) {
	uint16_t* buf = (uint16_t*) dest;
	for(size_t i = 0; i < n; i++) buf[i] = val;
	return dest;
}

void *memset_32(void *dest, uint32_t val, size_t n) {
	uint32_t* buf = (uint32_t*) dest;
	for(size_t i = 0; i < n; i++) buf[i] = val;
	return dest;
}

#endif
