/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <timer.h>
#include <task.h>

volatile uint64_t timer_ticks = 0; // number of microseconds passed since the timer is initialized
timer_event_t timer_events[1024] = {{0, 0, NULL, 0, 0}};

void usleep_primitive(uint64_t usec) {
	uint64_t dest_tick = timer_ticks + usec;
	while(timer_ticks < dest_tick);
}

void usleep(uint64_t usec) {
	task_pause = 0;
	usleep_primitive(usec);
}

void sleep(uint32_t seconds) {
	uint64_t usec = seconds * 1000000;
	usleep(usec);
}

void ustall(uint64_t usec) {
	task_pause = 1;
	usleep_primitive(usec);
	task_pause = 0;
}

void stall(uint32_t seconds) {
	uint64_t usec = seconds * 1000000;
	ustall(usec);
}

/* NOTE: timer drivers have to use this now instead of manually adding the timer ticks in order for timer events to work */
void timer_add_ticks(uint64_t ticks) {
	timer_ticks += ticks;
	for(uint16_t i = 0; i < 1024; i++) {
		if((timer_events[i].active) && ((timer_events[i].begin_tick + timer_events[i].ticks) <= timer_ticks)) {
			if(timer_events[i].mode) timer_events[i].begin_tick = timer_ticks;
			else timer_events[i].active = 0;
			timer_events[i].func();
		}
	}
}

uint16_t timer_add_event(uint8_t mode, void (*func)(), uint64_t ticks) {
	for(uint16_t i = 0; i < 1024; i++) {
		if(!timer_events[i].active) {
			timer_events[i].mode = mode;
			timer_events[i].func = func;
			timer_events[i].ticks = (uint64_t) ticks;
			timer_events[i].begin_tick = (uint64_t) timer_ticks;
			timer_events[i].active = 1; // has to be set last
			return i;
		}
	}
	return 0xFFFF; // no available event slot
}

void timer_remove_event(uint16_t event) {
	timer_events[event].active = 0;
}
