/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vfs.h>
#include <assert.h>
#include <stdlib.h>

vfs_node_t *vfs_root = NULL;

uint32_t vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	if(node->read != 0) return node->read((void*) node, offset, size, buf);
	return 0;
}

uint32_t vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	if(node->write != 0) return node->write((void*) node, offset, size, buf);
	return 0;
}

void vfs_open(vfs_node_t *node, uint8_t flags) {
	(void) flags; // TODO: check if the caller have permission to read/write to the file
	if(node->open != 0) node->open((void*) node);
}

void vfs_close(vfs_node_t *node) {
	if(node->close != 0) node->close((void*) node);
}

struct dirent *vfs_readdir(vfs_node_t *node, uint32_t index) {
	if((node->flags & 0x07) == VFS_TYPE_DIR && node->readdir != 0) return node->readdir((void*) node, index);
	return NULL;
}

vfs_node_t *vfs_finddir(vfs_node_t *node, char *name) {
	if((node->flags & 0x07) == VFS_TYPE_DIR && node->finddir != 0) return node->finddir((void*) node, name);
	return NULL;
}

vfs_node_t *vfs_create(vfs_node_t *node, char *name) {
	if((node->flags & 0x07) == VFS_TYPE_DIR && node->create != 0) return node->create((void*) node, name);
	return NULL;
}

int vfs_delete(vfs_node_t *node) {
	if((node->flags & 0x07) != VFS_TYPE_DIR && node->delete != 0) return node->delete((void*) node);
	return 0;
}

vfs_node_t *vfs_mkdir(vfs_node_t *node, char *name) {
	if((node->flags & 0x07) == VFS_TYPE_DIR && node->mkdir != 0) return node->mkdir((void*) node, name);
	return 0;
}

int vfs_rmdir(vfs_node_t *node) {
	if((node->flags & 0x07) == VFS_TYPE_DIR && node->rmdir != 0) return node->rmdir((void*) node);
	return 0;
}

/* inode number allocation */
uint32_t *vfs_bitmap = NULL; // max size is 512MB
uint32_t vfs_bitmap_size = 0; // bitmap size in dwords

uint32_t vfs_inode_alloc(void) {
	/* get a free inode number */
	for(uint32_t i = 0; i < vfs_bitmap_size; i++) {
		for(uint8_t j = 0; j < 32; j++) {
			if(!(vfs_bitmap[i] & (1 << j)) && (i * 32 + j) != 0) {
				vfs_bitmap[i] |= (1 << j);
				return (i * 32 + j);
			}
		}
	}
	if(vfs_bitmap_size >= (((uint64_t) 0x100000000) / 32)) return 0; // no free inode number left
	vfs_bitmap = realloc(vfs_bitmap, ++vfs_bitmap_size * 4); // expand the bitmap
	vfs_bitmap[vfs_bitmap_size - 1] = 0;
	return (vfs_bitmap_size - 1) * 32;
}

void vfs_inode_dealloc(uint32_t inode) {
	/* free an inode number */
	assert(inode != 0); // we can never EVER deallocate the root node
	assert((inode / 32) < vfs_bitmap_size);
	vfs_bitmap[inode / 32] &= ~(1 << (inode % 32));
	/* check if we can shrink the inode bitmap down (TODO: test) */
	uint32_t new_size = vfs_bitmap_size;
	for(uint32_t i = new_size - 1; i > 0; i++) {
		if(vfs_bitmap[i] != 0) {
			new_size = i + 1;
			break;
		}
	}
	vfs_bitmap = realloc(vfs_bitmap, new_size * 4);
	vfs_bitmap_size = new_size;
}
