/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/acpi.h>
#include <lai/helpers/sci.h>
#include <i386-pc/pic.h>
#include <stdio.h>
#include <task.h>
#include <power.h>

uint8_t acpi_sci_irql = 0;
void (*acpi_sci_next_irqhandler)() = NULL;

void acpi_sci_handler(void) {
	pic_eoi(acpi_sci_irql);
	uint16_t event = lai_get_sci_event();
	// printf("Received SCI event 0x%x\n", event);
	asm("sti");
	if(event & ACPI_POWER_BUTTON) power_btnhandler();
	if(acpi_sci_next_irqhandler != NULL) (*acpi_sci_next_irqhandler)();
}
