/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/exceptions.h>
#include <i386-pc/idt.h>
#include <textmode.h>
#include <stdio.h>
#include <framebuffer.h>
#include <elf32.h>

struct exc_registers {
	uint16_t ds;
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
	uint32_t excno, errcode;
	uint32_t eip, cs, eflags, useresp, ss;
} __attribute__ ((packed));

static char *exc_names[32] = {
	"division by zero",
	"debug",
	"non-maskable interrupt",
	"breakpoint",
	"overflow",
	"bound range exceeded",
	"invalid opcode",
	"device not available",
	"double fault",
	"coprocessor segment overrun",
	"invalid TSS",
	"segment not present",
	"stack-segment fault",
	"general protection fault",
	"page fault",
	"reserved",
	"x87 floating-point exception",
	"alignment check",
	"machine check",
	"SIMD floating-point exception",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"reserved",
	"security exception",
	"reserved"
};

void exc_handler(struct exc_registers regs) {
	text_setbg(TEXT_RED); text_setfg(TEXT_WHITE);
	puts("!!! EXCEPTION CAUGHT !!!");
	printf("Exception %u (%s), error code 0x%x\n", regs.excno, exc_names[regs.excno], regs.errcode);
	printf("EAX = 0x%8x, EBX = 0x%8x, ECX = 0x%8x, EDX = 0x%8x\n", regs.eax, regs.ebx, regs.ecx, regs.edx);
	printf("ESI = 0x%8x, EDI = 0x%8x, ESP = 0x%8x, EBP = 0x%8x\n", regs.esi, regs.edi, regs.esp, regs.ebp);
	printf("CS = 0x%4x, DS = 0x%4x, SS = 0x%4x\n", regs.cs, regs.ds, regs.ss);
	printf("EFLAGS = 0x%8x, user ESP = 0x%8x\n", regs.eflags, regs.useresp);
	printf("EIP = 0x%8x ", regs.eip);
	uint32_t off = 0; const char *fname = elf32_addr2sym(regs.eip, &off);
	if(fname != NULL) printf("(%s + 0x%x)\n", fname, off);
	else putchar('\n');
	uint32_t cr0, cr2, cr3;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	asm volatile("mov %%cr2, %0" : "=r"(cr2));
	asm volatile("mov %%cr3, %0" : "=r"(cr3));
	printf("CR0 = 0x%8x, CR2 = 0x%8x, CR3 = 0x%8x\n", cr0, cr2, cr3);
	while(1);
}

extern void exception0();
extern void exception1();
extern void exception2();
extern void exception3();
extern void exception4();
extern void exception5();
extern void exception6();
extern void exception7();
extern void exception8();
extern void exception9();
extern void exception10();
extern void exception11();
extern void exception12();
extern void exception13();
extern void exception14();
extern void exception15();
extern void exception16();
extern void exception17();
extern void exception18();
extern void exception19();
extern void exception20();
extern void exception21();
extern void exception22();
extern void exception23();
extern void exception24();
extern void exception25();
extern void exception26();
extern void exception27();
extern void exception28();
extern void exception29();
extern void exception30();
extern void exception31();

void exc_init(void) {
	idt_add_entry(0, 0x08, (uintptr_t) exception0, IDT_TYPE_INT, 0x08);
	idt_add_entry(1, 0x08, (uintptr_t) exception1, IDT_TYPE_INT, 0x08);
	idt_add_entry(2, 0x08, (uintptr_t) exception2, IDT_TYPE_INT, 0x08);
	idt_add_entry(3, 0x08, (uintptr_t) exception3, IDT_TYPE_INT, 0x08);
	idt_add_entry(4, 0x08, (uintptr_t) exception4, IDT_TYPE_INT, 0x08);
	idt_add_entry(5, 0x08, (uintptr_t) exception5, IDT_TYPE_INT, 0x08);
	idt_add_entry(6, 0x08, (uintptr_t) exception6, IDT_TYPE_INT, 0x08);
	idt_add_entry(7, 0x08, (uintptr_t) exception7, IDT_TYPE_INT, 0x08);
	idt_add_entry(8, 0x08, (uintptr_t) exception8, IDT_TYPE_INT, 0x08);
	idt_add_entry(9, 0x08, (uintptr_t) exception9, IDT_TYPE_INT, 0x08);
	idt_add_entry(10, 0x08, (uintptr_t) exception10, IDT_TYPE_INT, 0x08);
	idt_add_entry(11, 0x08, (uintptr_t) exception11, IDT_TYPE_INT, 0x08);
	idt_add_entry(12, 0x08, (uintptr_t) exception12, IDT_TYPE_INT, 0x08);
	idt_add_entry(13, 0x08, (uintptr_t) exception13, IDT_TYPE_INT, 0x08);
	idt_add_entry(14, 0x08, (uintptr_t) exception14, IDT_TYPE_INT, 0x08);
	idt_add_entry(15, 0x08, (uintptr_t) exception15, IDT_TYPE_INT, 0x08);
	idt_add_entry(16, 0x08, (uintptr_t) exception16, IDT_TYPE_INT, 0x08);
	idt_add_entry(17, 0x08, (uintptr_t) exception17, IDT_TYPE_INT, 0x08);
	idt_add_entry(18, 0x08, (uintptr_t) exception18, IDT_TYPE_INT, 0x08);
	idt_add_entry(19, 0x08, (uintptr_t) exception19, IDT_TYPE_INT, 0x08);
	idt_add_entry(20, 0x08, (uintptr_t) exception20, IDT_TYPE_INT, 0x08);
	idt_add_entry(21, 0x08, (uintptr_t) exception21, IDT_TYPE_INT, 0x08);
	idt_add_entry(22, 0x08, (uintptr_t) exception22, IDT_TYPE_INT, 0x08);
	idt_add_entry(23, 0x08, (uintptr_t) exception23, IDT_TYPE_INT, 0x08);
	idt_add_entry(24, 0x08, (uintptr_t) exception24, IDT_TYPE_INT, 0x08);
	idt_add_entry(25, 0x08, (uintptr_t) exception25, IDT_TYPE_INT, 0x08);
	idt_add_entry(26, 0x08, (uintptr_t) exception26, IDT_TYPE_INT, 0x08);
	idt_add_entry(27, 0x08, (uintptr_t) exception27, IDT_TYPE_INT, 0x08);
	idt_add_entry(28, 0x08, (uintptr_t) exception28, IDT_TYPE_INT, 0x08);
	idt_add_entry(29, 0x08, (uintptr_t) exception29, IDT_TYPE_INT, 0x08);
	idt_add_entry(30, 0x08, (uintptr_t) exception30, IDT_TYPE_INT, 0x08);
	idt_add_entry(31, 0x08, (uintptr_t) exception31, IDT_TYPE_INT, 0x08);
}
