/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/gdt.h>

static gdtr_t gdtr;
static gdt_entry_t gdt_entries[8192];

void gdt_add_entry(uint8_t entry, uintptr_t base, uintptr_t limit, uint8_t access, uint8_t flags) {
	gdt_entries[entry].base_0_15 = (uint16_t) (base & 0xFFFF);
	gdt_entries[entry].base_16_23 = (uint8_t) ((base >> 16) & 0xFF);
	gdt_entries[entry].base_24_31 = (uint8_t) ((base >> 24) & 0xFF);
	gdt_entries[entry].limit_0_15 = (uint16_t) (limit & 0xFFFF);
	gdt_entries[entry].limit_16_19 = (uint8_t) ((limit >> 16) & 0x0F);
	gdt_entries[entry].access = access;
	gdt_entries[entry].flags = flags;
}

void gdt_init(void) {
	gdt_add_entry(0, 0, 0, 0, 0); // null entry
	gdt_add_entry(1, 0, 0xFFFFF, 0x9A, 0x0C); // 0x08: code segment
	gdt_add_entry(2, 0, 0xFFFFF, 0x92, 0x0C); // 0x10: data segment
	gdtr.size = sizeof(gdt_entry_t) * 8192 - 1;
	gdtr.offset = (uint32_t) &gdt_entries;
	asm volatile("cli");
	asm volatile("lgdt (%%eax)" : : "a"(&gdtr));
	asm volatile("mov $0x10, %eax");
	asm volatile("mov %ax, %ds");
	asm volatile("mov %ax, %es");
	asm volatile("mov %ax, %fs");
	asm volatile("mov %ax, %gs");
	asm volatile("mov %ax, %ss");
	asm volatile("jmp $0x08, $.flush");
	asm volatile(".flush:");
	asm volatile("ret");
}
