/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <io.h>

void outb(uintptr_t port, uint8_t val) {
    asm volatile ("outb %0, %1" : : "a"(val), "Nd"((uint16_t) port));
}

uint8_t inb(uintptr_t port) {
    uint8_t ret;
    asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"((uint16_t) port));
    return ret;
}

void io_wait(void) {
	asm volatile ("outb %%al, $0x80" : : "a"(0));
}

uint16_t inw(uintptr_t port) {
	uint16_t ret;
	asm volatile("inw %w1,%0" : "=a"(ret) : "Nd"((uint16_t) port));
	return ret;
}

uint32_t inl(uintptr_t port) {
	uint32_t ret;
	asm volatile("inl %w1,%0" : "=a"(ret) : "Nd"((uint16_t) port));
	return ret;
}

void outw(uintptr_t port, uint16_t val) {
	asm volatile ("outw %w0, %w1" : : "a"(val), "Nd"((uint16_t) port));
}

void outl(uintptr_t port, uint32_t val) {
	asm volatile ("outl %0, %w1" : : "a"(val), "Nd"((uint16_t) port));
}
