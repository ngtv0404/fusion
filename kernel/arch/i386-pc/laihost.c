/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <lai/host.h>
#include <lai/core.h>
#include <acpispec/tables.h>
#include <i386-pc/pci.h>
#include <i386-pc/acpi.h>
#include <i386-pc/pic.h>
#include <io.h>
#include <paging.h>
#include <timer.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void *laihost_malloc(size_t size) {
	return malloc(size);
}

void *laihost_realloc(void *ptr, size_t size) {
	return realloc(ptr, size);
}

void laihost_free(void *ptr) {
	free(ptr);
}

void *laihost_map(size_t address, size_t count) {
	uintptr_t vaddr = paging_find_free(count);
	if(!vaddr) return NULL;
	paging_map(address, vaddr, count, 0, 1);
	return (void*) (vaddr + (address & 0xFFF));
}

void laihost_unmap(void *pointer, size_t count) {
	paging_unmap((uintptr_t) pointer, count);
}

void laihost_sleep(uint64_t ms) {
	usleep(ms * 1000);
}

void laihost_outb(uint16_t port, uint8_t val) {
	outb(port, val);
}

void laihost_outw(uint16_t port, uint16_t val) {
	outw(port, val);
}

void laihost_outd(uint16_t port, uint32_t val) {
	outl(port, val);
}

uint8_t laihost_inb(uint16_t port) {
	return inb(port);
}

uint16_t laihost_inw(uint16_t port) {
	return inw(port);
}

uint32_t laihost_ind(uint16_t port) {
	return inl(port);
}

void laihost_pci_writeb(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset, uint8_t val) {
	(void) seg;
	pci_config_write8(bus, slot, fun, offset, val);
}

void laihost_pci_writew(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset, uint16_t val) {
	(void) seg;
	pci_config_write16(bus, slot, fun, offset, val);
}

void laihost_pci_writed(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset, uint32_t val) {
	(void) seg;
	pci_config_write32(bus, slot, fun, offset, val);
}

uint8_t laihost_pci_readb(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset) {
	(void) seg;
	return pci_config_read8(bus, slot, fun, offset);
}

uint16_t laihost_pci_readw(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset) {
	(void) seg;
	return pci_config_read16(bus, slot, fun, offset);
}

uint32_t laihost_pci_readd(uint16_t seg, uint8_t bus, uint8_t slot, uint8_t fun, uint16_t offset) {
	(void) seg;
	return pci_config_read32(bus, slot, fun, offset);
}

__attribute__((noreturn)) void laihost_panic(const char *msg) {
	printf("LAI PANIC, SYSTEM HALTED:\n %s", msg);
	while(1);
}

void laihost_log(int level, const char *msg) {
	if(level == LAI_DEBUG_LOG) printf("LAI DEBUG: %s\n", msg);
	else if(level == LAI_WARN_LOG) printf("LAI WARNING: %s\n", msg);
}

static acpi_xsdp_t *rsdp = NULL;
static acpi_rsdt_t *rsdt = NULL;
static acpi_xsdt_t *xsdt = NULL;

void *acpi_find_sdt(const char *sig, uint32_t index) {
	uint32_t cnt = 0;
	acpi_header_t *ptr = NULL;
	if(rsdt != NULL) {
		for(uint32_t i = 0; i < (rsdt->header.length - sizeof(acpi_header_t)) / 4; i++) {
			ptr = (acpi_header_t*) laihost_map(rsdt->tables[i], sizeof(acpi_header_t));
			if(!strncmp(ptr->signature, sig, 4)) {
				if(cnt++ == index) {
					uint32_t t = ptr->length;
					laihost_unmap((void*) ptr, sizeof(acpi_header_t));
					ptr = laihost_map(rsdt->tables[i], t);
					return (void*) ptr;
				}
			}
			laihost_unmap((void*) ptr, sizeof(acpi_header_t));
		}
		return NULL;
	} else if(xsdt != NULL) {
		for(uint32_t i = 0; i < (xsdt->header.length - sizeof(acpi_header_t)) / 8; i++) {
			ptr = (acpi_header_t*) laihost_map(xsdt->tables[i], sizeof(acpi_header_t));
			if(!strncmp(ptr->signature, sig, 4)) {
				if(cnt++ == index) {
					uint32_t t = ptr->length;
					laihost_unmap((void*) ptr, sizeof(acpi_header_t));
					ptr = laihost_map(xsdt->tables[i], t);
					return (void*) ptr;
				}
			}
			laihost_unmap((void*) ptr, sizeof(acpi_header_t));
		}
		return NULL;
	} else return NULL;
}

void *laihost_scan(const char *signature, size_t index) {
	if(!strncmp(signature, "DSDT", 4)) {
		if(index) return NULL;
		acpi_fadt_t *fadt = (acpi_fadt_t*) acpi_find_sdt("FACP", 0);
		acpi_header_t *dsdt = (acpi_header_t*) laihost_map(fadt->dsdt, sizeof(acpi_header_t));
		uint32_t t = dsdt->length;
		laihost_unmap(dsdt, sizeof(acpi_header_t));
		dsdt = laihost_map(fadt->dsdt, t);
		laihost_unmap(fadt, fadt->header.length);
		return (void*) dsdt;
	} else return acpi_find_sdt(signature, index);
}

int laihost_init(void) {
	for(uint32_t i = 0x40E; i < 0x140E; i++) {
		if(!strncmp((char*) i, "RSD PTR ", 8)) {
			rsdp = (acpi_xsdp_t*) i;
			break;
		}
	}
	if(rsdp == NULL) {
		for(uint32_t i = 0xE0000; i < 0x100000; i++) {
			if(!strncmp((char*) i, "RSD PTR ", 8)) {
				rsdp = (acpi_xsdp_t*) i;
				break;
			}
		}
	}
	if(rsdp == NULL) return -1; // cannot find RSDP
	lai_set_acpi_revision(rsdp->revision);
	if(!rsdp->revision) {
		rsdt = laihost_map(rsdp->rsdt, sizeof(acpi_rsdt_t));
		uint32_t t = rsdt->header.length;
		laihost_unmap(rsdt, sizeof(acpi_rsdt_t));
		rsdt = laihost_map(rsdp->rsdt, t);
	} else {
		xsdt = laihost_map(rsdp->xsdt, sizeof(acpi_xsdt_t));
		uint32_t t = xsdt->header.length;
		laihost_unmap(xsdt, sizeof(acpi_xsdt_t));
		xsdt = laihost_map(rsdp->xsdt, t);
	}
	acpi_fadt_t *fadt = (acpi_fadt_t*) acpi_find_sdt("FACP", 0);
	acpi_sci_irql = fadt->sci_irq;
	printf("SCI IRQ %u\n", acpi_sci_irql);
	acpi_sci_next_irqhandler = pic_irqhandlers[acpi_sci_irql];
	pic_irqhandlers[acpi_sci_irql] = (void*) acpi_sci_handler;
	pic_mask(acpi_sci_irql, 0);
	laihost_unmap(fadt, fadt->header.length);
	return 0;
}
