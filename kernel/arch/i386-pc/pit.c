/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/pit.h>
#include <i386-pc/pic.h>
#include <timer.h>
#include <io.h>

void pit_set_freq(uint32_t freq) {
	uint32_t divisor = 1193180 / freq;
	outb(0x43, 0x36);
	outb(0x40, divisor & 0xFF);
	outb(0x40, (divisor >> 8) & 0xFF);
}

void pit_handler() {
	asm("sti");
	pic_eoi(0);
	timer_add_ticks(1000000); // since we are running this at 1KHz we have to compensate, if we were at 1MHz we would instead use timer_ticks++
}

void pit_init(void) {
	pic_irqhandlers[0] = pit_handler;
	pit_set_freq(1); // 1KHz
	pic_mask(0, 0); // unmask IRQ 0 in case it's masked
}
