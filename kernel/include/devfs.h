/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DEVFS_H
#define DEVFS_H

#include <stddef.h>
#include <stdint.h>
#include <vfs.h>

extern vfs_node_t *devfs_root;

/* magic values for determining if a device is being opened or not, stored in impl */
#define DEVFS_IMPL_OPEN								0x091019FE
#define DEVFS_IMPL_CLOSED							0x091019FF

int devfs_init(void);
vfs_node_t *devfs_register(char *name, vfs_read_t read, vfs_write_t write);
int devfs_unregister(vfs_node_t *device);
uint8_t devfs_isopen(vfs_node_t *device);

#endif
