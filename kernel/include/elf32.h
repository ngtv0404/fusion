/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELF32_H
#define ELF32_H

#include <stddef.h>
#include <stdint.h>

/* ELF header */
typedef struct {
	/* e_ident but broken down to its indexes */
	uint8_t ei_mag[4]; // 0x7F, "ELF"
	uint8_t ei_class; // file class
	uint8_t ei_data; // data encoding
	uint8_t ei_version; // file version
	uint8_t ei_pad[9]; // padding, ignored
	uint16_t e_type; // object file type
	uint16_t e_machine; // required architecture
	uint32_t e_version; // object file version
	uint32_t e_entry; // executable entry point
	uint32_t e_phoff; // program header table's file offset
	uint32_t e_shoff; // section header table's file offset
	uint32_t flags; // processor-specific flags
	uint16_t e_ehsize; // header size
	uint16_t e_phentsize; // program header table entry size
	uint16_t e_phnum; // number of program header table entries
	uint16_t e_shentsize; // section header table entry size
	uint16_t e_shnum; // number of section header table entries
	uint16_t e_shstrndx; // section name string table's section header table index
} __attribute__ ((packed)) elf32_hdr_t;

#define ELFMAG						"\x7f""ELF"				// expected magic value in ei_mag

/* possible ei_class values */
#define ELFCLASSNONE			0								// invalid class
#define ELFCLASS32				1								// 32-bit objects
#define ELFCLASS64				2								// 64-bit objects

/* possible ei_data values */
#define ELFDATANONE				0								// invalid data encoding
#define ELFDATA2LSB				1								// little endian 2's complement encoding
#define ELFDATA2MSB				2								// big endian 2's complement encoding

/* possible e_type values */
#define ET_NONE						0								// no file type
#define ET_REL						1								// relocatable file
#define ET_EXEC						2								// executable file
#define ET_DYN						3								// shared object file
#define ET_CORE						4								// core file
#define ET_LOPROC					0xff00					// processor-specific (from 0xff00 to 0xffff)
#define ET_HIPROC					0xffff					// processor-specific (from 0xff00 to 0xffff)

/* possible e_machine values */
#define EM_NONE						0								// no machine
#define EM_M32						1								// AT&T WE 32100
#define EM_SPARC					2								// SPARC
#define EM_386						3								// Intel 80386
#define EM_68K						4								// Motorola 68000
#define EM_88K						5								// Motorola 88000
#define EM_860						7								// Intel 80860
#define EM_MIPS						8								// MIPS RS3000

/* possible e_version values */
#define EV_NONE						0								// invalid version
#define EV_CURRENT				1								// current version

/* special section indexes */
#define SHN_UNDEF					0								// undefined, missing, irrelevant, or otherwise meaningless section reference
#define SHN_LORESERVE			0xff00					// lower bound of the range of reserved indexes
#define SHN_LOPROC				0xff00					// lower bound for processor-specific semantics
#define SHN_HIPROC				0xff1f					// upper bound for processor-specific semantics
#define SHN_ABS						0xfff1					// absolute values for the corresponding reference
#define SHN_COMMON				0xfff2					// common symbols
#define SHN_HIRESERVE			0xffff					// upper bound of the range of reserved indexes

/* section header */
typedef struct {
	uint32_t sh_name; // name of the section (index into the section header string table)
	uint32_t sh_type; // section's contents and semantics
	uint32_t sh_flags; // miscellaneous attributes
	uint32_t sh_addr; // start address of section
	uint32_t sh_offset; // section offset relative to file beginning
	uint32_t sh_size; // section size
	uint32_t sh_link; // section header table index link
	uint32_t sh_info; // extra information (section type specific)
	uint32_t sh_addralign; // address alignment constraints
	uint32_t sh_entsize; // size in bytes of each entry in the section
} __attribute__ ((packed)) elf32_shdr_t;

/* possible sh_type values */
#define SHT_NULL					0								// inactive section
#define SHT_PROGBITS			1								// program-defined information section
#define SHT_SYMTAB				2								// symbol table section
#define SHT_STRTAB				3								// string table section
#define SHT_RELA					4								// section containing relocation entries with explicit addends
#define SHT_HASH					5								// symbol hash table section
#define SHT_DYNAMIC				6								// section containing information for dynamic linking
#define SHT_NOTE					7								// file marking information section
#define SHT_NOBITS				8								// resembles SHT_PROGBITS, but occupies no space in the file
#define SHT_REL						9								// section containing relocation entries without explicit addends
#define SHT_SHLIB					10							// reserved section with unspecified semantics
#define SHT_DYNSYM				11							// symbol table section (for dynamic linking)
#define SHT_LOPROC				0x70000000			// lower bound for processor-specific sections
#define SHT_HIPROC				0x7fffffff			// upper bound for processor-specific sections
#define SHT_LOUSER				0x80000000			// lower bound for application-reserved sections
#define SHT_HIUSER				0xffffffff			// lower bound for application-reserved sections

/* possible sh_flags values */
#define SHF_WRITE					0x1							// section contains data that should be writable during process execution
#define SHF_ALLOC					0x2							// section occupies memory during process execution
#define SHF_EXECINSTR			0x4							// section contains executable machine instructions
#define SHF_MASKPROC			0xf0000000			// reserved for processor-specific semantics

/* symbol table entry */
#define STN_UNDEF					0								// symbol table index 0 both designates the first entry in the table and serves as the undefined symbol index
typedef struct {
	uint32_t st_name; // symbol name (index into symbol string table)
	uint32_t st_value; // associated symbol value (depends on context)
	uint32_t st_size; // symbol size
	uint8_t st_info; // type and binding attributes
	uint8_t st_other; // no defined meaning
	uint16_t st_shndx; // relevant section header table index
} __attribute__ ((packed)) elf32_sym_t;

/* macros for manipulating st_info */
#define ELF32_ST_BIND(info)								((info) >> 4)
#define ELF32_ST_TYPE(info)								((info) & 0xf)
#define ELF32_ST_INFO(bind, type)					(((bind) << 4) | ((type) & 0xf))

/* possible st_bind values */
#define STB_LOCAL					0								// local symbol
#define STB_GLOBAL				1								// global symbol
#define STB_WEAK					2								// weak symbol
#define STB_LOPROC				13							// lower bound for processor-specific symbols
#define STB_HIPROC				15							// upper bound for processor-specific symbols

/* possible st_type values */
#define STT_NOTYPE				0								// unspecified
#define STT_OBJECT				1								// symbol is associated with a data object
#define STT_FUNC					2								// symbol is associated with a function/executable code
#define STT_SECTION				3								// symbol is associated with a section
#define STT_FILE					4								// symbol's name gives the name of the source file associated with the object file
#define STT_LOPROC				13							// lower bound for processor-specific symbols
#define STT_HIPROC				15							// upper bound for processor-specific symbols

/* relocation entries */
typedef struct {
	uint32_t r_offset; // where to apply relocation
	uint32_t r_info; // symbol table index + type of relocation to apply
} __attribute__ ((packed)) elf32_rel_t;

typedef struct {
	uint32_t r_offset; // where to apply relocation
	uint32_t r_info; // symbol table index + type of relocation to apply
	int32_t r_addend; // constant addend used to compute the value to be stored into the relocatable field
} __attribute__ ((packed)) elf32_rela_t;

/* macros for manipulating r_info */
#define ELF32_R_SYM(i)										((i) >> 8)
#define ELF32_R_TYPE(i)										((i) & 0xff)
#define ELF32_R_INFO(s, t)								(((s) >> 8) | ((t) & 0xff))

/* relocation types (for 386 only) */
#define R_386_NONE					0							// no relocation
#define R_386_32						1
#define R_386_PC32					2
#define R_386_GOT32					3							// computes the distance from the pase of the GOT to the symbol's GOT entry
#define R_386_PLT32					4							// computes the address of the symbol's PLT entry and additionally instructs the link editor to build a PLT
#define R_386_COPY					5							// for dynamic linking
#define R_386_GLOB_DAT			6							// used to set a GOT entry to the address of the specified symbol
#define R_386_JMP_SLOT			7							// for dynamic linking
#define R_386_RELATIVE			8							// for dynamic linking
#define R_386_GOTOFF				9							// computes the difference between a symbol's value and the address of the GOT
#define R_386_GOTPC					10						// resembles R_386_PC32, except it uses the address of the GOT in its calculation

/* macros for calculating depending on relocation type */
#define DO_386_32(S, A)										((S) + (A))
#define DO_386_PC32(S, A, P)							((S) + (A) - (P))
#define DO_386_GOT32(G, A, P)							((G) + (A) - (P))
#define DO_386_PLT32(L, A, P)							((L) + (A) - (P))
#define DO_386_RELATIVE(B, A)							((B) + (A))
#define DO_386_GOTOFF(S, A, GOT)					((S) + (A) - (GOT))
#define DO_386_GOTPC(GOT, A, P)						((GOT) + (A) - (P))

/* file loading modes */
#define ELF32_MODE_BIT_EXPSYM							(1 << 0)				// export symbols
#define ELF32_MODE_BIT_LOAD								(1 << 1)				// load data into memory
#define ELF32_MODE_BIT_FIND_INIT					(1 << 2)				// find init()
#define ELF32_MODE_BIT_FIND_MAIN					(1 << 3)				// find main()
#define ELF32_MODE_EXPORT_SYMS						(ELF32_MODE_BIT_EXPSYM)																										// export symbols only (useful for loading kernel.sym)
#define ELF32_MODE_LOAD_MODS							(ELF32_MODE_BIT_LOAD | ELF32_MODE_BIT_EXPSYM | ELF32_MODE_BIT_FIND_INIT)	// for loading modules
#define ELF32_MODE_LOAD_BINS							(ELF32_MODE_BIT_LOAD | ELF32_MODE_BIT_FIND_MAIN)													// for loading binaries

/* symbol table entry */
typedef struct {
	char name[120]; // symbol name
	uint16_t type; // symbol type (STT_OBJECT or STT_FUNC)
	uint16_t bind; // symbol bind type (STB_GLOBAL, STB_WEAK or STB_LOCAL)
	uint32_t ptr; // pointer to symbol
	uint32_t size; // symbol size
} __attribute__ ((packed)) elf32_gstab_t;

uintptr_t elf32_load(void *file, uint32_t size, uint32_t mode, uintptr_t *new_addr);
const char *elf32_addr2sym(uintptr_t addr, uint32_t *offset);

#endif
