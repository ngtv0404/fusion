/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <stddef.h>
#include <stdint.h>
#include <vfs.h>

#define rgb(r, g, b)			((r << 16) | (g << 8) | b)

/* framebuffer color bitfield configuration */
typedef struct {
	uint8_t bytes; // bytes per pixel
	uint8_t endian; // endianness (0 = little, 1 = big)
	/* bitfield configuration */
	uint8_t r_start, r_bits;
	uint8_t g_start, g_bits;
	uint8_t b_start, b_bits;
} __attribute__ ((packed)) fb_color_t;

/* framebuffer device structure */
typedef struct {
	uint32_t width, height, bpp, pitch; // pitch = bytes per line
	fb_color_t color; // color bitfield
	uint8_t *buffer; // framebuffer
} __attribute__ ((packed)) fb_device_t;

extern fb_device_t *fb_default;
extern vfs_node_t *fbdev;

void fb_putpixel(fb_device_t *dev, uint32_t x, uint32_t y, uint32_t color); // put pixel at specific coordinates
uint32_t fb_getpixel(fb_device_t *dev, uint32_t x, uint32_t y); // get the color of a specific pixel
void fb_fill(fb_device_t *dev, uint32_t color);
void fb_drawline(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color);
void fb_drawrect(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color);
void fb_fillrect(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color);
void fb_drawbitmap(fb_device_t *dev, uint32_t x, uint32_t y, uint32_t *bitmap, uint32_t width, uint32_t height);
void fb_scrollup(fb_device_t *dev, uint32_t lines, uint32_t y0, uint32_t y1);
int fb_vfs_init(void);

#endif
