/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
	int32.h - Protected Mode BIOS Call Functionality
	Copyright 2009 Napalm@rohitab.com.
*/

#ifndef I386_PC_INT32_H
#define I386_PC_INT32_H

#include <stddef.h>
#include <stdint.h>

/* int32 registers structure */
typedef struct __attribute__ ((packed)) {
	uint16_t di, si, bp, sp, bx, dx, cx, ax;
	uint16_t gs, fs, es, ds, eflags;
} int32_regs_t;

extern void int32(uint8_t intnum, int32_regs_t *regs);

#endif
