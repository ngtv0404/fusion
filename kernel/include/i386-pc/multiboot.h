/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef I386_PC_MULTIBOOT_H
#define I386_PC_MULTIBOOT_H

#include <stddef.h>
#include <stdint.h>

/* boot_device structure */
typedef struct {
	uint8_t part3, part2, part1, drive;
} __attribute__ ((packed)) mb_bootdev_t;

/* module structure */
typedef struct {
	uint32_t mod_start, mod_end; // the start and end addresses of the module
	uint8_t *string; // pointer to an arbitrary string to be associated with the module
	uint32_t reserved;
} __attribute__ ((packed)) mb_module_t;

/* a.out symbol table structure */
typedef struct {
	uint32_t tabsize; // size parameter found at the beginning of the symbol section
	uint32_t strsize; // size parameter found at the beginning of the string section
	uint32_t addr; // the physical address of the size of an array of a.out format nlist structures, followed immediately by the array, then the set of strings plus sizeof(uint32_t) and the set of strings
	uint32_t reserved;
} __attribute__ ((packed)) mb_aout_symtab_t;

/* ELF section header table structure */
typedef struct {
	uint32_t num; // the number of entries
	uint32_t size; // the size of each entry
	uint32_t addr; // where the section header table is
	uint32_t shndx; // the string table index
} __attribute__ ((packed)) mb_elf_shtab_t;

/* memory map table entry structure */
#define MB_MMAP_TYPE_AVAILABLE					1
#define MB_MMAP_TYPE_ACPI						3
#define MB_MMAP_TYPE_NVS						4 // NVS stands for Non-volatile Sleeping (needs to be preserved on hibernation)
#define MB_MMAP_TYPE_DEFECTIVE					5
typedef struct {
	uint32_t size; // the size of the structure - 4
	/* starting address */
	uint32_t base_addr_lo;
	uint32_t base_addr_hi;
	/* length */
	uint32_t length_lo;
	uint32_t length_hi;
	uint32_t type; // memory region type
	/*
	 * possible types:
	 * 1 - available RAM
	 * 3 - usable memory holding ACPI information
	 * 4 - reserved memory which needs to be preserved on hibernation
	 * 5 - defective RAM
	 */
} __attribute__ ((packed)) mb_mmap_t;

/* drive structure */
#define MB_DRIVE_MODE_CHS						0
#define MB_DRIVE_MODE_LBA						1
typedef struct {
	uint32_t size; // the size of the structure
	uint8_t drive_number; // BIOS drive number
	uint8_t drive_mode; // drive mode: 0 = CHS, 1 = LBA
	/* drive geometry detected by the BIOS */
	uint16_t drive_cylinders;
	uint8_t drive_heads;
	uint8_t drive_sectors;
	uint16_t drive_ports[]; // I/O ports used for the drive in the BIOS code
} __attribute__ ((packed)) mb_drive_t;

/* APM table structure */
typedef struct {
	uint16_t version; // version number
	uint16_t cseg; // the protected mode 32-bit code segment
	uint32_t offset; // the offset of the entry pointer
	uint16_t cseg_16; // the protected mode 16-bit code segment
	uint16_t dseg; // the protected mode 16-bit data segment
	uint16_t flags; // the flags
	uint16_t cseg_len; // the length of the protected mode 32-bit code segment
	uint16_t cseg_16_len; // the length of the protected mode 16-bit code segment
	uint16_t dseg_len; // the length of the protected mode 16-bit data segment
} __attribute__ ((packed)) mb_apm_t;

/* VBE control information structure */
typedef struct {
	uint8_t signature[4]; // must be "VESA"
	uint16_t version;
	uint32_t oem; // segment:offset pointer to OEM name
	uint32_t capabilities;
	uint32_t modes; // segment:offset pointer to list of supported video modes
	uint16_t mem; // memory size in 64K blocks
	uint16_t oem_sw_rev;
	uint32_t oem_vendor; // segment:offset pointer to card vendor string
	uint32_t oem_prod_name; // segment:offset pointer to model name
	uint32_t oem_prod_rev; // segment:offset pointer to model revision
	uint8_t reserved[222];
	uint8_t oem_data[256];
} __attribute__ ((packed)) mb_vbe_ctrlinfo_t;

/* VBE mode information structure */
typedef struct {
	uint16_t attribs;
	uint8_t win_a, win_b;
	uint16_t granularity;
	uint16_t win_size;
	uint16_t seg_a, seg_b;
	uint32_t win_func_ptr;
	uint16_t pitch;

	uint16_t width, height;
	uint8_t w_char, y_char, planes, bpp, banks;
	uint8_t mem_model, bank_size, img_pages;
	uint8_t reserved;

	uint8_t r_mask, r_pos;
	uint8_t g_mask, g_pos;
	uint8_t b_mask, b_pos;
	uint8_t res_mask, res_pos;
	uint8_t direct_color_attribs;

	uint32_t physbase;
	uint32_t offscreenmem_off;
	uint32_t offscreenmem_size;
	uint8_t reserved2[206];
} __attribute__ ((packed)) mb_vbe_modeinfo_t;

/* indexed color information structure */
typedef struct {
	uint8_t red_value, green_value, blue_value;
} __attribute__ ((packed)) mb_indexed_palette_t;
typedef struct {
	mb_indexed_palette_t *framebuffer_palette_addr; // the address of the color palette
	uint16_t framebuffer_palette_addr_num_colors; // the number of color palettes
} __attribute__ ((packed)) mb_indexed_colorinfo_t;

/* direct RGB color information structure */
typedef struct {
	uint8_t framebuffer_red_field_position;
	uint8_t framebuffer_red_mask_size;
	uint8_t framebuffer_green_field_position;
	uint8_t framebuffer_green_mask_size;
	uint8_t framebuffer_blue_field_position;
	uint8_t framebuffer_blue_mask_size;
} __attribute__ ((packed)) mb_direct_colorinfo_t;

/* Multiboot boot information structure */
typedef struct {
	uint32_t flags;

	/* present if flags[0] is set */
	uint32_t mem_lower; // the amount of lower memory in kilobytes
	uint32_t mem_upper; // the amount of upper memory in kilobytes

	/* present if flags[1] is set */
	mb_bootdev_t boot_device; // the BIOS disk device the bootloader loaded the kernel from

	/* present if flags[2] is set */
	uint8_t *cmdline; // the physical address of the command line to be passed to the kernel

	/* present if flags[3] is set */
	uint32_t mods_count; // the number of modules loaded
	mb_module_t *mods_addr; // the physical address of the first module structure

	/* present if flags[4] or flags[5] is set */
	union {
		mb_aout_symtab_t aout_symtab; // a.out symbol table, used if flags[4] is set
		mb_elf_shtab_t elf_shtab; // ELF section header table, used if flags[5] is set
	} syms;

	/* present if flags[6] is set */
	uint32_t mmap_length; // the total size of the memory map table
	mb_mmap_t *mmap_addr; // the address to the first memory map table entry

	/* present if flags[7] is set */
	uint32_t drives_length; // the total size of the drive structures
	mb_drive_t *drives_addr; // the address of the first drive structure

	/* present if flags[8] is set */
	uint32_t config_table; // the address of the ROM configuration table returned by the GET CONFIGURATION BIOS call

	/* present if flags[9] is set */
	uint8_t *boot_loader_name; // the physical address of the name of the bootloader booting the kernel

	/* present if flags[10] is set */
	mb_apm_t *apm_table; // the physical of the APM table

	/* present if flags[11] is set */
	mb_vbe_ctrlinfo_t *vbe_control_info; // the physical address of the VBE control information returned by VBE function 00h
	mb_vbe_modeinfo_t *vbe_mode_info; // the physical address of the VBE mode information returned by the VBE function 01h
	uint16_t vbe_mode; // current video mode in the format specified in VBE 3.0
	uint16_t vbe_interface_seg; // protected mode interface segment
	uint16_t vbe_interface_off; // protected mode interface offset
	uint16_t vbe_interface_len; // protected mode interface length

	/* present if flags[12] is set */
	uint32_t framebuffer_addr_lo; // framebuffer physical address
	uint32_t framebuffer_addr_hi; // shouldn't be used
	uint32_t framebuffer_pitch; // framebuffer pitch in bytes
	uint32_t framebuffer_width; // framebuffer width
	uint32_t framebuffer_height; // framebuffer height
	uint8_t framebuffer_bpp; // bits per pixel (set to 16 if framebuffer_type = 2)
	uint8_t framebuffer_type; // 0 = indexed color, 1 = direct RGB color, 2 = EGA-standard text mode
	union {
		mb_indexed_colorinfo_t indexed_color; // indexed color information, used if framebuffer_type = 0
		mb_direct_colorinfo_t direct_rgb; // direct RGB color information, used if framebuffer_type = 1
	} color_info;
} __attribute__ ((packed)) mb_info_t;

extern uintptr_t mb_start; // start address
extern uintptr_t mb_end; // end address
extern mb_info_t *mb_info; // information structure

void mb_init(void); // relocate the Multiboot tables and put the start and end addresses in mb_start and mb_end

#endif
