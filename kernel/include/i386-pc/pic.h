/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef I386_PC_PIC_H
#define I386_PC_PIC_H

#include <stddef.h>
#include <stdint.h>

extern uint8_t pic_offset0, pic_offset1;
extern volatile uint64_t pic_spurious_7, pic_spurious_15;
extern void (*pic_irqhandlers[16])();

void pic_eoi(uint8_t irq);
void pic_remap(uint8_t offset0, uint8_t offset1);
void pic_mask(uint8_t irq, uint8_t state);
void pic_mask_all(void);
void pic_unmask_all(void);
uint16_t pic_get_ocw3(uint8_t ocw3);
uint16_t pic_get_irr(void);
uint16_t pic_get_isr(void);
void pic_setup_handler(void);

#endif
