/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KBLAYOUTS_QWERTY_US_H
#define KBLAYOUTS_QWERTY_US_H

#include <keyboard.h>

kb_modifier_t qwerty_us_shift = {
	(1 << 11) | (1 << 1),
	(1 << 11),
	47,
	{
		{'`', '~'}, {'1', '!'}, {'2', '@'}, {'3', '#'}, {'4', '$'}, {'5', '%'}, {'6', '^'}, {'7', '&'}, {'8', '*'}, {'9', '('}, {'0', ')'}, {'-', '_'}, {'=', '+'},
		{'q', 'Q'}, {'w', 'W'}, {'e', 'E'}, {'r', 'R'}, {'t', 'T'}, {'y', 'Y'}, {'u', 'U'}, {'i', 'I'}, {'o', 'O'}, {'p', 'P'}, {'[', '{'}, {']', '}'},
		{'a', 'A'}, {'s', 'S'}, {'d', 'D'}, {'f', 'F'}, {'g', 'G'}, {'h', 'H'}, {'j', 'J'}, {'k', 'K'}, {'l', 'L'}, {';', ':'}, {'\'', '"'}, {'\\', '|'},
		{'z', 'Z'}, {'x', 'X'}, {'c', 'C'}, {'v', 'V'}, {'b', 'B'}, {'n', 'N'}, {'m', 'M'}, {',', '<'}, {'.', '>'}, {'/', '?'}
	}
};

kb_modifier_t qwerty_us_caps = {
	(1 << 11) | (1 << 1),
	(1 << 1),
	26,
	{
		{'q', 'Q'}, {'w', 'W'}, {'e', 'E'}, {'r', 'R'}, {'t', 'T'}, {'y', 'Y'}, {'u', 'U'}, {'i', 'I'}, {'o', 'O'}, {'p', 'P'},
		{'a', 'A'}, {'s', 'S'}, {'d', 'D'}, {'f', 'F'}, {'g', 'G'}, {'h', 'H'}, {'j', 'J'}, {'k', 'K'}, {'l', 'L'},
		{'z', 'Z'}, {'x', 'X'}, {'c', 'C'}, {'v', 'V'}, {'b', 'B'}, {'n', 'N'}, {'m', 'M'}
	}
};

kb_modifier_t qwerty_us_caps_shift = {
	(1 << 11) | (1 << 1),
	(1 << 11) | (1 << 1),
	21,
	{
		{'`', '~'}, {'1', '!'}, {'2', '@'}, {'3', '#'}, {'4', '$'}, {'5', '%'}, {'6', '^'}, {'7', '&'}, {'8', '*'}, {'9', '('}, {'0', ')'}, {'-', '_'}, {'=', '+'},
		{'[', '{'}, {']', '}'},
		{';', ':'}, {'\'', '"'}, {'\\', '|'},
		{',', '<'}, {'.', '>'}, {'/', '?'}
	}
};

kb_modifier_t qwerty_us_no_numlock = {
	(1 << 0),
	0,
	15,
	{
		{KB_KPAD_0, KB_INSERT}, {KB_KPAD_DOT, KB_DEL},
		{KB_KPAD_1, KB_END}, {KB_KPAD_2, KB_ARROW_DOWN}, {KB_KPAD_3, KB_PAGE_DOWN},
		{KB_KPAD_4, KB_ARROW_LEFT}, {KB_KPAD_5, 0}, {KB_KPAD_6, KB_ARROW_RIGHT},
		{KB_KPAD_7, KB_HOME}, {KB_KPAD_8, KB_ARROW_UP}, {KB_KPAD_9, KB_PAGE_UP}, {KB_KPAD_PLUS, 0},
		{KB_KPAD_SLASH, 0}, {KB_KPAD_STAR, 0}, {KB_KPAD_DASH, 0}
	}
};

kb_modifier_t qwerty_us_numlock = {
	(1 << 0),
	(1 << 0),
	15,
	{
		{KB_KPAD_SLASH, '/'}, {KB_KPAD_STAR, '*'}, {KB_KPAD_DASH, '-'},
		{KB_KPAD_7, '7'}, {KB_KPAD_8, '8'}, {KB_KPAD_9, '9'}, {KB_KPAD_PLUS, '+'},
		{KB_KPAD_4, '4'}, {KB_KPAD_5, '5'}, {KB_KPAD_6, '6'},
		{KB_KPAD_1, '1'}, {KB_KPAD_2, '2'}, {KB_KPAD_3, '3'},
		{KB_KPAD_0, '0'}, {KB_KPAD_DOT, '.'}
	}
};

kb_layout_t qwerty_us = {
	"QWERTY (United States/ANSI-INCITS 154-1988 (R1999))",
	{
		KB_ESC, KB_F1, KB_F2, KB_F3, KB_F4, KB_F5, KB_F6, KB_F7, KB_F8, KB_F9, KB_F10, KB_F11, KB_F12, KB_PRTSC, KB_SCROLL_LOCK, KB_PAUSE, KB_WWW_FAV, KB_MM_COMP, KB_MM_MAIL, KB_MM_CALC, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		'`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', KB_INSERT, KB_HOME, KB_PAGE_UP, KB_NUM_LOCK, KB_KPAD_SLASH, KB_KPAD_STAR, KB_KPAD_DASH, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		'\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', KB_DEL, KB_END, KB_PAGE_DOWN, KB_KPAD_7, KB_KPAD_8, KB_KPAD_9, KB_KPAD_PLUS, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		KB_CAPS_LOCK, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '\\', KB_MM_PREV, KB_MM_NEXT, KB_MM_VUP, KB_KPAD_4, KB_KPAD_5, KB_KPAD_6, '\r', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		KB_SHIFT_L, 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', KB_SHIFT_R, '\b', KB_MM_PLAY, KB_MM_STOP, KB_MM_VDOWN, KB_KPAD_1, KB_KPAD_2, KB_KPAD_3, KB_KPAD_DOT, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		KB_CTRL_L, KB_WIN_L, KB_ALT_L, ' ', KB_ALT_R, KB_WIN_R, KB_MENU, KB_CTRL_R, KB_ARROW_UP, KB_ARROW_DOWN, KB_ARROW_LEFT, KB_ARROW_RIGHT, '\r', KB_WWW_BACK, KB_WWW_NEXT, KB_MM_MUTE, KB_KPAD_0, KB_WWW_REFRESH, KB_WWW_STOP, KB_WWW_SEARCH, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, KB_MM_MSEL, KB_WWW_HOME, 0, KB_ACPI_POWER, KB_ACPI_SLEEP, KB_ACPI_WAKE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	},
	5,
	{&qwerty_us_shift, &qwerty_us_caps, &qwerty_us_caps_shift, &qwerty_us_no_numlock, &qwerty_us_numlock}
};

#endif
