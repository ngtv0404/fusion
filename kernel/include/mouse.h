/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MOUSE_H
#define MOUSE_H

#include <stddef.h>
#include <stdint.h>

#define MOUSE_LEFT					(1 << 0)
#define MOUSE_MID					(1 << 2)
#define MOUSE_RIGHT					(1 << 1)

extern volatile uint32_t mouse_x, mouse_y;
extern volatile uint8_t mouse_buttons; // xxxxxMRL (L = left button, M = middle button, R = right button)

void mouse_update(int32_t dx, int32_t dy);

#endif
