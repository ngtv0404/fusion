/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PFA_H
#define PFA_H

#include <stddef.h>
#include <stdint.h>

extern uintptr_t pfa_size;
extern uintptr_t *pfa_bitmap;

void pfa_setframe(uintptr_t frame);
void pfa_clrframe(uintptr_t frame);
uint8_t pfa_statframe(uintptr_t frame);
void pfa_reserve(uintptr_t addr, uintptr_t size);
void pfa_free(uintptr_t addr, uintptr_t size);
uintptr_t pfa_freeframe(void);
void pfa_init(uintptr_t size);

#endif
