/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STDIO_H
#define STDIO_H

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <vfs.h>

extern vfs_node_t *stdin;
extern vfs_node_t *stdout;

int putchar(uint32_t c);
int puts(const char *str);
int vsprintf(char *str, const char *format, va_list params);
int sprintf(char *str, const char *format, ...);
int printf(const char *format, ...);
uint32_t getc(vfs_node_t *stream);
char *gets(char *str);
int stdio_vfs_init(void);

#endif
