/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SYNC_H
#define SYNC_H

#include <stddef.h>
#include <stdint.h>

/* comment these lines out if you choose to provide your own architecture-specific implementation */
#define COMMON_MUTEX
#define COMMON_SEMAPHORE
#define COMMON_SPINLOCK

/* mutex */
void *mutex_create(void);
void mutex_destroy(void *mutex);
int mutex_acquire(void *mutex);
void mutex_release(void *mutex);

/* semaphore */
void *sem_create(void);
void sem_destroy(void *sem);
void sem_wait(void *sem);
void sem_signal(void *sem);

/* spinlock */
void *splk_create(void);
void splk_destroy(void *splk);
void splk_acquire(void *splk);
void splk_release(void *splk);

#endif
