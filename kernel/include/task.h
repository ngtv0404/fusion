/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TASK_H
#define TASK_H

#include <stddef.h>
#include <stdint.h>

extern uintptr_t task_running;
extern uintptr_t task_kernel;
extern uint8_t task_pause;

void task_init(void); // do all your necessary multitasking initialization (i.e. creating kernel task)
uintptr_t task_create(void (*main)(), uint32_t flags, uintptr_t pstruct); // create task with start address, architecture-specific flags and paging structure ID
void task_remove(uintptr_t task); // remove task
void task_switch(uintptr_t task); // switch immediately to a specified task
void task_switch_pid(uint32_t pid); // same as above, but with PID
void task_yield(void); // switch to next task and move the current task to the end of the queue

/* PID management */
#define TASK_MAXPROCS								16384
extern uintptr_t task_pidmap[TASK_MAXPROCS];
uint32_t task_pid_alloc(uintptr_t task); // allocate a PID and register task
void task_pid_free(uint32_t pid); // free PID
uintptr_t task_pid2task(uint32_t pid); // get task from PID
uint32_t task_task2pid(uintptr_t task); // get PID from task
uint32_t task_getpid(void); // get PID of current task

#endif
