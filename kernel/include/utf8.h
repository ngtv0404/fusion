#ifndef UTF8_H
#define UTF8_H

#include <stddef.h>
#include <stdint.h>

uint8_t utf8_bytes(uint8_t first); // get the number of bytes the UTF-8 character needs based on the first byte
uint8_t utf8_bytes32(uint32_t c); // get the number of bytes the UTF-32 character needs when converted to UTF-8
uint32_t utf8_translate_32(uint8_t *c); // translate UTF-8 chracter to UTF-32
uint8_t utf8_translate_8(uint32_t c, uint8_t *out); // translate UTF-32 character to UTF-8, write it in out, and return the number of bytes written
/* these functions behave the same as their string.h counterpart */
size_t utf8_strlen(const uint8_t *s);
char* utf8_strcpy(char* dest, const char* src);
int utf8_strcmp(const char* str1, const char* str2);
int utf8_strncmp(const void* str1, const void* str2, size_t n);

#endif
