/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VFS_H
#define VFS_H

#include <stddef.h>
#include <stdint.h>

#define VFS_TYPE_FILE									0x01
#define VFS_TYPE_DIR									0x02
#define VFS_TYPE_SYMLINK							0x03
#define VFS_TYPE_PIPE									0x04
#define VFS_TYPE_BLKDEV								0x05
#define VFS_TYPE_CHRDEV								0x06
#define VFS_TYPE_MNTPOINT							0x08 // also a bitmask that can be OR'd with VFS_TYPE_DIR

#define VFS_OPEN_READ									(1 << 0)
#define VFS_OPEN_WRITE								(1 << 1)

typedef uint32_t (*vfs_read_t)(struct vfs_node*, uint32_t, uint32_t, uint8_t*);
typedef uint32_t (*vfs_write_t)(struct vfs_node*, uint32_t, uint32_t, uint8_t*);
typedef void (*vfs_open_t)(struct vfs_node*);
typedef void (*vfs_close_t)(struct vfs_node*);
typedef struct dirent *(*vfs_readdir_t)(struct vfs_node*, uint32_t);
typedef struct vfs_node *(*vfs_finddir_t)(struct vfs_node*, char*);
typedef struct vfs_node *(*vfs_create_t)(struct vfs_node*, char*);
typedef int (*vfs_delete_t)(struct vfs_node*);
typedef struct vfs_node *(*vfs_mkdir_t)(struct vfs_node*, char*);
typedef int (*vfs_rmdir_t)(struct vfs_node*);

typedef struct vfs_node {
	char name[256];
	uint32_t mask, uid, gid;
	uint32_t flags;
	uint32_t len;
	uintptr_t inode;
	uint32_t impl;
	vfs_read_t read;
	vfs_write_t write;
	vfs_open_t open;
	vfs_close_t close;
	vfs_readdir_t readdir;
	vfs_finddir_t finddir;
	vfs_create_t create;
	vfs_delete_t delete;
	vfs_mkdir_t mkdir;
	vfs_rmdir_t rmdir;
	struct vfs_node *ptr;
} __attribute__ ((packed)) vfs_node_t;

struct dirent {
	char name[256];
	uint32_t inode;
};

extern vfs_node_t *vfs_root; // root node, has no name

uint32_t vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf);
uint32_t vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf);
void vfs_open(vfs_node_t *node, uint8_t flags);
void vfs_close(vfs_node_t *node);
struct dirent *vfs_readdir(vfs_node_t *node, uint32_t index);
vfs_node_t *vfs_finddir(vfs_node_t *node, char *name);
vfs_node_t *vfs_create(vfs_node_t *node, char *name);
int vfs_delete(vfs_node_t *node);
vfs_node_t *vfs_mkdir(vfs_node_t *node, char *name);
int vfs_rmdir(vfs_node_t *node);
uint32_t vfs_inode_alloc(void);
void vfs_inode_dealloc(uint32_t inode);

#endif
