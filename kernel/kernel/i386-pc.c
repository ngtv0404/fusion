/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/gdt.h>
#include <i386-pc/idt.h>
#include <i386-pc/exceptions.h>
#include <i386-pc/pic.h>
#include <i386-pc/pit.h>
#include <i386-pc/multiboot.h>
#include <i386-pc/ps2.h>
#include <i386-pc/acpi.h>
#include <stdio.h>
#include <textmode.h>
#include <i386-pc/pci.h>
#include <lai/helpers/sci.h>
#include <mmap.h>
#include <i386-pc/vbe.h>
#include <framebuffer.h>
#include <timer.h>

uintptr_t initrd_addr, initrd_size;

void kainit(void) {
	gdt_init();
	idt_init();
	exc_init();
	text_clear();
	pic_remap(0x20, 0x28);
	pic_setup_handler();
	pit_init();
	mb_init();
	int ret = ps2_init();
	printf("ps2_init() returned %d\n", ret);
	if(ret != 0) {
		printf("PS/2 initialization failed! Going into an infinite loop...\n");
		while(1);
	}
	if(mb_info->mods_count == 0) {
		printf("No initrd loaded!\n");
		while(1);
	}
	printf("PCI initialization returned %d\n", pci_init());
	initrd_addr = mb_info->mods_addr->mod_start;
	initrd_size = mb_info->mods_addr->mod_end - mb_info->mods_addr->mod_start;
}

extern int laihost_init();

void kainit_stage2(void) {
	vbe_init(0, 0, 0);
	text_fb = fb_default;
	text_clear();
	/* load memory map */
	mb_mmap_t *ent = mb_info->mmap_addr;
	while((uintptr_t) ent < ((uintptr_t) mb_info->mmap_addr + mb_info->mmap_length) && !ent->base_addr_hi) {
		printf("Multiboot entry 0x%x: start 0x%x, len 0x%x, type %u\n", ent, ent->base_addr_lo, ent->length_lo, ent->type);
		mmap_register(ent->base_addr_lo, ent->length_lo, (ent->type == 1) ? MMAP_AVAILABLE : MMAP_RESERVED);
		ent = (mb_mmap_t*) ((uintptr_t) ent + ent->size + sizeof(ent->size));
	}
	/* initialize LAI */
	printf("LAI (Lightweight ACPI Implementation) - Copyright (C) 2018-2020 the lai authors\n");
	printf("laihost_init() returned %d\n", laihost_init());
	lai_create_namespace();
	printf("lai_enable_acpi(0) returned %d\n", lai_enable_acpi(0));
}
